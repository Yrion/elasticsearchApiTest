package com.wyq.elasticsearch.bean;


import lombok.Data;

import java.io.Serializable;

/**
 * 接口返回值
 *
 * @author wyq
 */
@Data
public class ResultVo<T> implements Serializable {

    private Integer resultCode;

    private String resultMessage;

    private Long count;

    private T data;


    /**
     * 空的构造方法
     */
    public ResultVo() {
    }

    /**
     * 构造方法
     *
     * @param resultCode
     * @param resultMessage
     */
    public ResultVo(Integer resultCode, String resultMessage) {
        this.resultCode = resultCode;
        this.resultMessage = resultMessage;
    }

    /**
     * 请求成功
     *
     * @param data
     * @param <T>
     * @return
     */
    public static <T> ResultVo<T> success(T data, long size) {
        ResultVo<T> resultVo = new ResultVo<>();
        resultVo.setResultCode(ResultEnum.SUCCESS.getCode());
        resultVo.setResultMessage(ResultEnum.SUCCESS.getMessage());
        resultVo.setCount(size);
        resultVo.setData(data);
        return resultVo;
    }

    /**
     * 请求成功
     *
     * @param data
     * @param <T>
     * @return
     */
    public static <T> ResultVo<T> success(T data) {
        ResultVo<T> resultVo = new ResultVo<>();
        resultVo.setResultCode(ResultEnum.SUCCESS.getCode());
        resultVo.setResultMessage(ResultEnum.SUCCESS.getMessage());
        resultVo.setData(data);
        return resultVo;
    }

    /**
     * 请求成功
     *
     * @param <T>
     * @return
     */
    public static <T> ResultVo<T> success() {
        ResultVo<T> resultVo = new ResultVo<>(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getMessage());
        return resultVo;
    }

    /**
     * 请求失败
     *
     * @param <T>
     * @return
     */
    public static <T> ResultVo<T> failed() {
        ResultVo<T> resultVo = new ResultVo<>(ResultEnum.ERROR.getCode(), ResultEnum.ERROR.getMessage());
        return resultVo;
    }

    /**
     * 请求失败
     *
     * @param data
     * @param <T>
     * @return
     */
    public static <T> ResultVo<T> failed(T data) {
        ResultVo<T> resultVo = new ResultVo<>();
        resultVo.setResultCode(ResultEnum.ERROR.getCode());
        resultVo.setResultMessage(ResultEnum.ERROR.getMessage());
        resultVo.setData(data);
        return resultVo;
    }

    /**
     * 缺少必要参数
     *
     * @param <T>
     * @return
     */
    public static <T> ResultVo<T> missParameters() {
        ResultVo<T> resultVo = new ResultVo<>();
        resultVo.setResultCode(ResultEnum.missNecessaryParameters.getCode());
        resultVo.setResultMessage(ResultEnum.missNecessaryParameters.getMessage());
        resultVo.setData(null);
        return resultVo;
    }

    /**
     * 程序内部错误
     *
     * @param <T>
     * @return
     */
    public static <T> ResultVo<T> innerError() {
        ResultVo<T> resultVo = new ResultVo<>();
        resultVo.setResultCode(ResultEnum.internalError.getCode());
        resultVo.setResultMessage(ResultEnum.internalError.getMessage());
        resultVo.setData(null);
        return resultVo;
    }

}
