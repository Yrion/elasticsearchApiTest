package com.wyq.elasticsearch.bean;

import lombok.Data;
import org.elasticsearch.common.geo.GeoPoint;

import java.io.Serializable;


/**
 * @Author: wyq
 * @Desc: person实体
 * @Date: 2019/11/28 18:24
 **/
@Data
public class Person implements Serializable {

    private String id;

    private String name;

    private Integer age;

    private String birthday;

    private Hobby hobby;

    private Position position;

    private int provinceId;

    private String role;


}
