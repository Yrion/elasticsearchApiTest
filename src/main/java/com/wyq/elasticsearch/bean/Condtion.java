package com.wyq.elasticsearch.bean;

import java.util.Map;

/**
 * @Author: wyq
 * @Desc:
 * @Date: 2019/11/29 12:41
 **/
public class Condtion {

    /**
     * 搜索条件
     */
    private Map<String,String> condition;

    /**
     * 排序 1：倒序 2：正序
     */
    private int sort;

    /**
     * 排序 1：按照分数  2：按照年龄
     */
    private int orderBy;

    /**
     * 当前页
     */
    private int pageIndex;

    /**
     * 分页大小
     */
    private int pageSize;

}
