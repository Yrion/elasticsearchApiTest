package com.wyq.elasticsearch.bean;

import lombok.Getter;

/**
 * 返回enum
 * @author 作者
 * @date 2019年3月29日14:14:40
 */
@Getter
public enum ResultEnum {

    SUCCESS(1, "请求成功"),
    ERROR(0, "请求失败"),
    missNecessaryParameters(2, "缺少必要参数"),
    internalError(500, "程序内部错误");


    private Integer code;

    private String message;

    ResultEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }


}
