package com.wyq.elasticsearch.bean;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Author: wyq
 * @Desc: 爱好
 * @Date: 2019/11/28 18:25
 **/
@Data
public class Hobby implements Serializable {

    private List<Sports> sports;

    private List<Subject> subjects;
}
