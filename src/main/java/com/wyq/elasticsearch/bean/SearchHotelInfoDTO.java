package com.wyq.elasticsearch.bean;

import lombok.Data;

import java.util.Map;

/**
 * @Author: wyq
 * @Desc: 搜索酒店信息请求实体
 * @Date: 2019/11/27 9:48
 **/
@Data
public class SearchHotelInfoDTO {

    private Integer pageIndex;

    private Integer pageSize;

    private Integer orderBy;

    private Integer sort;

    private Map<String,String> condition;

}
