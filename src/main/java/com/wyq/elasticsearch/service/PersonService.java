package com.wyq.elasticsearch.service;

import com.wyq.elasticsearch.bean.Person;
import com.wyq.elasticsearch.bean.SearchPersonResult;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public interface PersonService {


    /**
     * 检查index是否存在
     *
     * @param indexName
     * @return
     */
    boolean checkIndexExist(String indexName);


    /**
     * 单个添加
     *
     * @param person
     * @return
     */
    boolean singleAdd(Person person, String indexName);


    /**
     * 单个更新
     *
     * @param person
     * @return
     */
    boolean singleUpdate(Person person, String indexName);


    /**
     * 批量添加
     *
     * @param people
     * @return
     */
    boolean batchAdd(List<Person> people, String indexName);

    /**
     * 批量更新
     *
     * @param people
     * @return
     */
    boolean batchUpdate(List<Person> people, String indexName);

    /**
     * 删除索引
     *
     * @param indexName 索引名
     * @return
     */
    boolean deleteIndex(String indexName);

    /**
     * 获取所有数据
     *
     * @param indexName
     * @return
     */
    SearchPersonResult getAll(String indexName);

    /**
     * 根据人名查找人
     *
     * @return
     */
    Person searchByName(String indexName, String personName);


    /**
     * 以经纬度为中心根据指定距离搜索
     *
     * @param lat
     * @param lon
     * @param indexName
     * @param distance
     * @return
     */
    Person geoSearch(Double lat, Double lon, String indexName, long distance);


    /**
     * 根据学科类型在指定的区间进行查询
     * @param minScore 最低分数
     * @param maxScore 最高分数
     * @param indexName 索引名
     * @param subjectType 学科类型
     * @return
     */
    SearchPersonResult searchByScoreRange(Integer minScore,Integer maxScore,String indexName,Integer subjectType);


    /**
     * 多条件搜索
     *
     * @param condition 搜索条件
     * @param pageIndex 分页下标
     * @param pageSize  分页大小
     * @param orderBy   排序根据
     * @return
     */
    SearchPersonResult multiSearch(Integer pageIndex, Integer pageSize, Map<String, String> condition, Integer orderBy, Integer sort);


}
