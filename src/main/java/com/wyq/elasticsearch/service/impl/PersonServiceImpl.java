package com.wyq.elasticsearch.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.wyq.elasticsearch.bean.Person;
import com.wyq.elasticsearch.bean.SearchPersonResult;
import com.wyq.elasticsearch.service.ElasticSearchService;
import com.wyq.elasticsearch.service.PersonService;
import org.apache.commons.text.StringEscapeUtils;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @Author: wyq
 * @Desc: person服务
 * @Date: 2019/11/29 14:15
 **/
@Service
public class PersonServiceImpl extends ElasticSearchService implements PersonService {

    /**
     * 检查index是否存在
     *
     * @param indexName
     * @return
     */
    public boolean checkIndexExist(String indexName) {
        return checkIndex(indexName);
    }

    /**
     * 单条添加
     *
     * @param person
     * @param indexName
     * @return
     */
    @Override
    public boolean singleAdd(Person person, String indexName) {
        return insert(indexName, person);
    }


    /**
     * 单条更新
     *
     * @param person
     * @param indexName
     * @return
     */
    @Override
    public boolean singleUpdate(Person person, String indexName) {
        return updateByIndex(indexName, person);
    }


    /**
     * 批量添加
     *
     * @param people
     * @return
     */
    @Override
    public boolean batchAdd(List<Person> people, String indexName) {

        final BulkRequest request = new BulkRequest();
        if (CollectionUtil.isEmpty(people)) {
            return false;
        }
        for (Person document : people) {
            request.add(getInsertSource(document, indexName));
        }
        return bulk(request);
    }


    /**
     * 批量更新
     *
     * @param people
     * @return
     */
    @Override
    public boolean batchUpdate(List<Person> people, String indexName) {
        final BulkRequest request = new BulkRequest();
        if (CollectionUtil.isEmpty(people)) {
            return false;
        }
        for (Person document : people) {
            request.add(getUpdateSource(document, indexName));
        }
        return bulk(request);
    }


    /**
     * 根据索引名删除索引,谨慎操作,也会删掉数据
     *
     * @param indexName 索引名
     * @return
     */
    @Override
    public boolean deleteIndex(String indexName) {
        return delete(indexName);
    }


    /**
     * 根据名字搜索
     *
     * @param indexName
     * @param personName
     * @return
     */
    @Override
    public Person searchByName(String indexName, String personName) {
        final Map<Integer, Object> returnJson = search(null, null, "name", personName, indexName);
        List<Person> people = null;
        if (MapUtil.isNotEmpty(returnJson)) {
            final String personInfoJson = (String) returnJson.get(2);
            if (StrUtil.isNotEmpty(personInfoJson)) {
                final String jsonStr = StringEscapeUtils.unescapeJava(personInfoJson);
                people = JSONUtil.toList(JSONUtil.parseArray(jsonStr), Person.class);
            }
        }
        return CollectionUtil.isEmpty(people) ? null : people.get(0);
    }


    /**
     * 地理位置搜索
     *
     * @param lat
     * @param lon
     * @param indexName
     * @param distance
     * @return
     */
    @Override
    public Person geoSearch(Double lat, Double lon, String indexName, long distance) {
        final Map<Integer, Object> resultJson = locationSearch(indexName, null, null, lat, lon, String.valueOf(distance));
        List<Person> people = null;
        if (MapUtil.isNotEmpty(resultJson)) {
            final String personInfoJson = (String) resultJson.get(2);
            if (StrUtil.isNotEmpty(personInfoJson)) {
                people = JSONUtil.toList(JSONUtil.parseArray(StringEscapeUtils.unescapeJava(personInfoJson)), Person.class);
            }
        }
        return CollectionUtil.isNotEmpty(people) ? people.get(0) : null;
    }

    /**
     * 根据分数范围查询
     *
     * @param minScore    最低分数
     * @param maxScore    最高分数
     * @param indexName   索引名
     * @param subjectType 学科类型
     * @return
     */
    @Override
    public SearchPersonResult searchByScoreRange(Integer minScore, Integer maxScore, String indexName, Integer subjectType) {
        final BoolQueryBuilder subjectAndScoreBuilder = getSubjectAndScoreBuilder(String.valueOf(subjectType), String.valueOf(minScore), String.valueOf(maxScore));
        try {
            SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
            searchSourceBuilder.query(subjectAndScoreBuilder);
            final Map<Integer, Object> resultMap = searchAndGetResult(searchSourceBuilder, indexName);
            List<Person> people = Collections.EMPTY_LIST;
            Long count = 0L;
            if (MapUtil.isNotEmpty(resultMap)) {
                String personInfoJson = (String) resultMap.get(2);
                count = (Long) resultMap.get(1);
                if (StrUtil.isNotEmpty(personInfoJson)) {
                    people = JSONUtil.toList(JSONUtil.parseArray(StringEscapeUtils.unescapeJava(personInfoJson)), Person.class);
                }
            }
            return new SearchPersonResult(people, count);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }


    /**
     * 获取所有数据
     *
     * @param indexName
     * @return
     */
    @Override
    public SearchPersonResult getAll(String indexName) {
        final Map<Integer, Object> returnJson = matchAllQuery(indexName, null, null);
        String personInfoJson = null;
        long count = 0;
        List<Person> people = Collections.EMPTY_LIST;
        if (MapUtil.isNotEmpty(returnJson)) {
            personInfoJson = (String) returnJson.get(2);
            count = (Long) returnJson.get(1);
            if (StrUtil.isNotEmpty(personInfoJson)) {
                people = JSONUtil.toList(JSONUtil.parseArray(StringEscapeUtils.unescapeJava(personInfoJson)), Person.class);
            }
        }
        return new SearchPersonResult(people, count);
    }


    /**
     * 多条件搜索
     *
     * @param condition 搜索条件
     * @param pageIndex 分页下标
     * @param pageSize  分页大小
     * @param orderBy   排序根据
     * @param sort      是否降序
     * @return
     */
    @Override
    public SearchPersonResult multiSearch(Integer pageIndex, Integer pageSize, Map<String, String> condition, Integer orderBy, Integer sort) {
        SearchPersonResult searchPersonResult = null;
        try {
            final Map<Integer, Object> resultMap = boolSearch(pageIndex, pageSize, condition, orderBy, sort);
            final String hotelInfoJson = (String) resultMap.get(2);
            final Long count = (Long) resultMap.get(1);
            List<Person> people = Collections.EMPTY_LIST;
            if (StrUtil.isNotEmpty(hotelInfoJson)) {
                people = JSONUtil.toList(JSONUtil.parseArray(StringEscapeUtils.unescapeJava(hotelInfoJson)), Person.class);
            }
            searchPersonResult.setCount(count);
            searchPersonResult.setPeople(people);
        } catch (Exception ex) {
        }
        return searchPersonResult;
    }


}
