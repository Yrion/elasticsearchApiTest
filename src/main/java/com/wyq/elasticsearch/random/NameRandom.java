package com.wyq.elasticsearch.random;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: wyq
 * @Desc: 名字随机
 * @Date: 2019/11/29 9:46
 **/
public class NameRandom {

    public static String[] jobs = {"程序员", "教师", "公务员", "外卖员", "研究员", "厨师", "司机", "售货员", "会计", "医生"};

    public static String[] sports = {"羽毛球", "棒球", "篮球", "足球", "手球", "曲棍球", "垒球", "乒乓球", "网球", "排球", "地掷球", "藤球", "门球", "毽球", "壁球", "台球", "高尔夫球", "保龄球", "沙壶球", "橄榄球", "体操", "蹦床", "技巧", "健美", "健美操", "体育舞蹈", "木兰拳（含木兰剑", "木兰扇等）", "舍宾运动", "运动保健按摩", "花样滑冰", "冰上舞蹈", "冰球", "速度滑冰", "滑雪","田径", "越野赛", "滑板", "轮滑", "自行车（含山地车", "独轮车等）", "滑水", "塞艇", "皮滑艇", "救生（含水上救生）；赛马", "举重", "击剑", "拳击", "柔道", "摔跤", "武术（含散打）", "跆拳道", "空手道", "健身气功；国际象棋", "中国象棋", "围棋", "桥牌；钓鱼", "信鸽", "舞龙", "舞狮", "龙舟", "风筝", "拔河；飞镖", "秋千", "航海模型", "航空模型", "车辆模型", "航天模型", "无线电", "定向运动"};


    public static String[] subjects = {"哲学", "经济学", "法学", "教育学", "文学", "历史学", "理学", "工学"};

    public static Map<String, String> subjectMap = new HashMap<>();

    public static Map<Integer, String> sportsMap = new HashMap<>();


    static {
        subjectMap.put("10", subjects[0]);
        subjectMap.put("20", subjects[1]);
        subjectMap.put("30", subjects[2]);
        subjectMap.put("40", subjects[3]);
        subjectMap.put("50", subjects[4]);
        subjectMap.put("60", subjects[5]);
        subjectMap.put("70", subjects[6]);
        subjectMap.put("80", subjects[7]);


        for (int i = 0; i < sports.length; i++) {
            sportsMap.put(i, sports[i]);
        }
    }

}
