package com.wyq.elasticsearch.random;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.json.JSONUtil;
import com.wyq.elasticsearch.bean.*;

import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Author: wyq
 * @Desc:
 * @Date: 2019/11/28 19:53
 **/
public class RandomPerson {

    public static void main(String[] args) {

        List<Person> people = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            final Person person = new Person();
            person.setId(IdUtil.fastUUID());
            person.setName(CreatName.getChineseName());
            person.setAge(RandomUtil.randomInt(16, 60));
            String str = "1992-10-01";
            final DateTime parse = DateUtil.parse(str, DatePattern.NORM_DATE_PATTERN);
            person.setBirthday(DateUtil.format(RandomUtil.randomDate(parse, DateField.DAY_OF_YEAR, 300, 10000), DatePattern.NORM_DATE_PATTERN));
            person.setHobby(getHobby());
            person.setPosition(getPosition());
            person.setProvinceId(RandomUtil.randomInt(1, 8));
            person.setRole(NameRandom.jobs[RandomUtil.randomInt(NameRandom.jobs.length - 1)]);
            people.add(person);
        }
        System.out.println(JSONUtil.toJsonStr(people));


    }

    private static Position getPosition() {
        final double lat = RandomUtil.randomDouble(1, 120);
        final double lon = RandomUtil.randomDouble(1, 60);
        return new Position(lat, lon);
    }


    /**
     * 创建爱好
     *
     * @return
     */
    private static Hobby getHobby() {
        final Hobby hobby = new Hobby();
        final List<Sports> sports = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            final Sports sports1 = new Sports();
            final String sport = NameRandom.sports[RandomUtil.randomInt(NameRandom.sports.length - 1)];
            sports1.setSportName(sport);
            sports1.setSportType(getValueBySportType(sport));
            sports.add(sports1);
        }
        hobby.setSports(sports);

        final List<Subject> subjects = new ArrayList<>();
        for (int i = 0; i < NameRandom.subjects.length; i++) {
            final Subject subject = new Subject();
            final String subject1 = NameRandom.subjects[i];
            subject.setSubjectName(subject1);
            subject.setSubjectType(Integer.parseInt(getValueByType(subject1)));
            subject.setSubjectScore(RandomUtil.randomDouble(0, 100, 2, RoundingMode.DOWN));
            subjects.add(subject);
        }
        hobby.setSubjects(subjects);
        return hobby;
    }


    /**
     * 根据type获取value
     *
     * @param subject
     * @return
     */
    private static String getValueByType(String subject) {
        for (Map.Entry<String, String> entry : NameRandom.subjectMap.entrySet()) {
            if (entry.getValue().equals(subject)) {
                return entry.getKey();
            }
        }
        return null;
    }


    private static Integer getValueBySportType(String subject) {
        for (Map.Entry<Integer, String> entry : NameRandom.sportsMap.entrySet()) {
            if (entry.getValue().equals(subject)) {
                return entry.getKey();
            }
        }
        return null;
    }
}
